<?php

/**
 * Implements hook_rules_data_info().
 */
function ss_rules_rules_data_info() {
  return array(
    'settings_share_fetch_request' => array(
      'label' => t('Settings Share Request'),
      'group' => t('Settings Share'),
      'wrap' => true,
      'property info' => ss_rules_property_fetch_request_info(),
    ),
  );
}

/**
 * Implements hook_rules_event_info().
 */
function ss_rules_rules_event_info() {
    $items = array(
        'settings_share_fetch_request' => array(
            'label' => t('Settings has been fetched by a remote Drupal site.'),
            'group' => t('Settings Share'),
            'variables' => array(
                'request' => array(
                    'type' => 'settings_share_fetch_request',
                    'label' => 'Settings Share Request',
                ),
            ),
        ),
    );

    return $items;
}

function ss_rules_property_fetch_request_info() {
  return array(
        'name' => array(
          'type' => 'text',
          'label' => t('Setting name'),
        ),
        'type' => array(
          'type' => 'text',
          'label' => ('Setting type'),
        ),
  );
}
