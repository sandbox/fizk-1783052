<?php

function ss_security_admin() {
    $form = array();

    $form['modules'] = array(
        '#type' => 'fieldset',
        '#title' => 'Modules',
        '#description' => 'Restrict by module',
        '#collapsed' => false,
        '#collapsible' => true,
    );
    $form['types'] = array(
        '#type' => 'fieldset',
        '#title' => 'Setting Types',
        '#description' => 'Restrict by setting type',
        '#collapsed' => false,
        '#collapsible' => true,
    );
    $form['names'] = array(
        '#type' => 'fieldset',
        '#title' => 'Disabled Settings',
        '#description' => 'Restrict by setting name',
        '#collapsed' => false,
        '#collapsible' => true,
    );

    $modules = module_invoke_all('settings_share_info');
    ksort($modules);

    foreach ($modules as $module => $info) {
        $form['modules']['ss_security_enable_module_' . $module] = array(
            '#type' => 'checkbox',
            '#title' => $info['name'],
            '#description' => 'Enable the ' . $info['name'] . ' module.',
            '#default_value' => variable_get('ss_security_enable_module_' . $module, 1),
        );

        if (isset($info['types']) && is_array($info['types'])) {
            foreach ($info['types'] as $type => $type_info) {
                $form['types']['ss_security_enable_type_' . $type] = array(
                    '#type' => 'checkbox',
                    '#title' => $type_info['name'] . ' settings.',
                    '#description' => 'Enable access to ' . $type_info['name'] . ' settings.',
                    '#default_value' => variable_get('ss_security_enable_type_' . $type, 1),
                );
            }
        }
    }

    return system_settings_form($form);
}
