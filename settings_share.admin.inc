<?php

function settings_share_admin() {
    $form = array();

    $form['local'] = array(
        '#type' => 'fieldset',
        '#title' => 'Local Sharing',
        '#collapsed' => false,
        '#collapsible' => true,
    );
    $form['local']['settings_share_enable_local_sharing'] = array(
        '#type' => 'checkbox',
        '#title' => 'Enable sharing',
        '#description' => 'Share settings on this site with other Drupal sites in your network that have your API key.',
        '#default_value' => variable_get('settings_share_enable_local_sharing', 0),
    );
    $form['local']['settings_share_local_public_key'] = array(
        '#type' => 'textfield',
        '#title' => 'Public Key',
        '#description' => 'The public key used to identify this site to other Drupal sites in your network.',
        '#default_value' => variable_get('settings_share_local_public_key', ''),
    );
    $form['local']['settings_share_local_secret_key'] = array(
        '#type' => 'textfield',
        '#title' => 'Secret Key',
        '#description' => 'The secret key used to authenticate other Drupal sites in your network.',
        '#default_value' => variable_get('settings_share_local_secret_key', ''),
    );

    $form['remote'] = array(
        '#type' => 'fieldset',
        '#title' => 'Remote Sharing',
        '#collapsed' => false,
        '#collapsible' => true,
    );
    $form['remote']['settings_share_enable_remote_sharing'] = array(
        '#type' => 'checkbox',
        '#title' => 'Fetch settings from remote site',
        '#description' => 'Fetch settings from a remote Drupal site.',
        '#default_value' => variable_get('settings_share_enable_remote_sharing', 0),
    );
    $form['remote']['settings_share_remote_domain'] = array(
        '#type' => 'textfield',
        '#title' => 'Remote Domain',
        '#description' => 'The domain name of the remote site you would like to retrieve settings from.'
                        . '<br>It\'s more secure to use a HTTPs url if possible (i.e. <b>https</b>://example.com instead of <b>http</b>://example.com)',
        '#default_value' => variable_get('settings_share_remote_domain', ''),
    );
    $form['remote']['settings_share_remote_public_key'] = array(
        '#type' => 'textfield',
        '#title' => 'Remote Public Key',
        '#description' => 'The remote site\'s public key.',
        '#default_value' => variable_get('settings_share_remote_public_key', ''),
    );
    $form['remote']['settings_share_remote_secret_key'] = array(
        '#type' => 'textfield',
        '#title' => 'Remote Secret Key',
        '#description' => 'The secret key used to authenticate other Drupal sites in your network.',
        '#default_value' => variable_get('settings_share_remote_secret_key', ''),
    );

    return system_settings_form($form);
}

function settings_share_admin_validate(&$form, &$form_state) {
    if ($form_state['values']['settings_share_enable_local_sharing'] == true) {
        if (empty($form_state['values']['settings_share_local_public_key'])) {
            form_set_error('settings_share_local_public_key', 'The public key cannot be empty.');
        } else if (strstr($form_state['values']['settings_share_local_public_key'], ' ') !== false) {
            form_set_error('settings_share_local_public_key', 'The public key cannot contain whitespace.');
        } else if (strlen($form_state['values']['settings_share_local_public_key']) < SETTINGS_SHARE_MIN_KEY_LENGTH) {
            form_set_error('settings_share_local_public_key', 'The public key must be at least ' . SETTINGS_SHARE_MIN_KEY_LENGTH . ' characters and be composed of numbers and letters.');
        }

        if (empty($form_state['values']['settings_share_local_secret_key'])) {
            form_set_error('settings_share_local_secret_key', 'The secret key cannot be empty.');
        } else if (strstr($form_state['values']['settings_share_local_secret_key'], ' ') !== false) {
            form_set_error('settings_share_local_secret_key', 'The secret key cannot contain whitespace.');
        } else if (strlen($form_state['values']['settings_share_local_secret_key']) < SETTINGS_SHARE_MIN_KEY_LENGTH) {
            form_set_error('settings_share_local_secret_key', 'The secret key must be at least ' . SETTINGS_SHARE_MIN_KEY_LENGTH . ' characters and be composed of numbers and letters.');
        }
    }

    if ($form_state['values']['settings_share_enable_remote_sharing'] == true) {
        if (empty($form_state['values']['settings_share_remote_domain']))
            form_set_error('settings_share_remote_domain', 'The remote domain cannot be empty.');

        if (empty($form_state['values']['settings_share_remote_public_key'])) {
            form_set_error('settings_share_remote_public_key', 'The remote public key cannot be empty.');
        } else if (strstr($form_state['values']['settings_share_remote_public_key'], ' ') !== false) {
            form_set_error('settings_share_remote_public_key', 'The remote public key cannot contain whitespace.');
        } else if (strlen($form_state['values']['settings_share_remote_public_key']) < SETTINGS_SHARE_MIN_KEY_LENGTH) {
            form_set_error('settings_share_remote_public_key', 'The remote public key must be at least ' . SETTINGS_SHARE_MIN_KEY_LENGTH . ' characters and be composed of numbers and letters.');
        }

        if (empty($form_state['values']['settings_share_remote_secret_key'])) {
            form_set_error('settings_share_remote_secret_key', 'The remote secret key cannot be empty.');
        } else if (strstr($form_state['values']['settings_share_remote_secret_key'], ' ') !== false) {
            form_set_error('settings_share_remote_secret_key', 'The remote secret key cannot contain whitespace.');
        } else if (strlen($form_state['values']['settings_share_remote_secret_key']) < SETTINGS_SHARE_MIN_KEY_LENGTH) {
            form_set_error('settings_share_remote_secret_key', 'The remote secret key must be at least ' . SETTINGS_SHARE_MIN_KEY_LENGTH . ' characters and be composed of numbers and letters.');
        }
    }
}
