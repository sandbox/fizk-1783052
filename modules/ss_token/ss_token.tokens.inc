<?php

/**
 * Implements hook_token_info().
 */
function ss_token_token_info() {
    $request = array();
    $type = array(
            'name' => t('Settings Share Request'),
            'description' => t('Tokens related to a Settings Share request.'),
            'needs-data' => 'settings_share_fetch_request',
            );

    $request['name'] = array(
            'name' => t("Setting Name"),
            'description' => t("The setting name."),
            );
    $request['type'] = array(
            'name' => t("Setting Type"),
            'description' => t("The setting type."),
            );

    return array(
            'types' => array('settings_share_fetch_request' => $type),
            'tokens' => array('settings_share_fetch_request' => $request),
            );
}

/**
 * Implements hook_tokens().
 */
function ss_token_tokens($type, $tokens, array $data = array(), array $options = array()) {
    $replacements = array();

    if ($type == 'settings_share_fetch_request' && !empty($data['settings_share_fetch_request'])) {
        $request = $data['settings_share_fetch_request'];
        foreach ($tokens as $name => $original) {
            switch ($name) {
                case 'name':
                    $replacements[$original] = !empty($request->name) ? $request->name : '';
                    break;
                case 'type':
                    $replacements[$original] = !empty($request->type) ? $request->type : '';
                    break;
            }
        }
    }

    return $replacements;
}
